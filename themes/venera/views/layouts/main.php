<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    
<head>
        <meta charset="utf-8">
        <title>Registro de escuelas y alumnos con Yii</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/theme_venera.css" media="all" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Abel:400|Oswald:300,400,700" media="all" rel="stylesheet" type="text/css" />
        <meta content="authenticity_token" name="csrf-param" />
<meta content="mcZQqRYWJr6dSGKE58lcmAA7hTBr7jwlE5wmc5oz3kY=" name="csrf-token" />
    
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/FortAwesome-Font-Awesome-ee55c85/css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="<?php echo Yii::app()->theme->baseUrl; ?>/FortAwesome-Font-Awesome-ee55c85/css/font-awesome.css" rel="stylesheet" type="text/css"></head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->
<header id='header'>
<div class='navbar navbar-fixed-top'>
<div class='navbar-inner'>
<div class='container'>
<a class='btn btn-navbar' data-target='.nav-collapse' data-toggle='collapse'>
<span class='icon-bar'></span>
<span class='icon-bar'></span>
<span class='icon-bar'></span>
</a>
<a href="#" class="brand">RegEscuelas</a>
<div class='nav-collapse subnav-collapse fixExpandMobile pull-right' id='top-navigation'>
<ul class='nav'>
<li class='active'>
<a href="<?php echo Yii::app()->createUrl('site/index') ?>">Inicio</a>
</li>
<li class='dropdown'>
<a href="basic_elements.html" class="dropdown-toggle" data-toggle="dropdown">Administrar</a>
<ul class='dropdown-menu'>
<li>
<a href="<?php echo Yii::app()->createUrl('escuela/admin') ?>">Escuelas</a></li>
<li>
<a href="<?php echo Yii::app()->createUrl('alumno/admin') ?>">Alumnos</a></li>
</ul>
</li>

<li class='dropdown'>
<a href="basic_elements.html" class="dropdown-toggle" data-toggle="dropdown">Ver</a>
<ul class='dropdown-menu'>
<li>
<a href="<?php echo Yii::app()->createUrl('escuela/index') ?>">Escuelas</a></li>
<li>
<a href="<?php echo Yii::app()->createUrl('alumno/vertodos') ?>">Alumnos</a></li>
</ul>
</li>


<li class=''>
<a href="<?php echo Yii::app()->createUrl('site/contact') ?>">Contáctanos</a>
</li>
</ul>
<div class='top-account-control visible-desktop'>
<a href="pricing_table.html" class="top-create-account">Regístrate</a>
<a href="#" class="top-sign-in">Entrar</a>

<div class='login-box'>
<a class='close login-box-close' href='#'>&times;</a>
<h4 class='login-box-head'>Login Form</h4>
<div class='control-group'>
<label>Username</label>
<input class='span2' placeholder='Input username...' type='text'>
</div>
<div class='control-group'>
<label>Password</label>
<input class='span2' placeholder='Input password...' type='text'>
</div>
<div class='login-actions'>
<button class='btn btn-primary'>Log Me In</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</header>
       
<div class="container">
		<?php echo $content ?>
</div> <!-- /container -->

<footer>
<div class='pre-footer'>
<div class='container'>
<div class='row'>
<div class='span4'>
<div class='footer-logo'>
<a>Theme<strong>Venera</strong></a>
</div>
<ul class='footer-address'>
<li>
<strong>Address:</strong>
2850 Collins ave<br/>
Camden, NJ 32432-1343, USA
</li>
<li>
<strong>Phone:</strong>
(324) 234-2343
</li>
<li>
<strong>Fax:</strong>
(324) 366-5423
</li>
</ul>
</div>
<div class='span4'>
<h5 class='footer-header'>Recent Posts</h5>
<ul class='footer-list'>
<li>
<a>Vestibulum auctor dapibus</a>
</li>
<li>
<a>Aliquam tincidunt mauris</a>
</li>
<li>
<a>Lorem ipsum dolor sit</a>
</li>
<li>
<a>Consectetur adipisicing elit</a>
</li>
</ul>
</div>
<div class='span4'>
<h5 class='footer-header'>Photostream</h5>
<ul class='footer-img-list thumbnails'>
<li class='span1'>
<a class='thumbnail'>
<img alt="8b9890" src="http://dummyimage.com/100x100/fff/8b9890" />
</a>
</li>
<li class='span1'>
<a class='thumbnail'>
<img alt="8b9890" src="http://dummyimage.com/100x100/fff/8b9890" />
</a>
</li>
<li class='span1'>
<a class='thumbnail'>
<img alt="8b9890" src="http://dummyimage.com/100x100/fff/8b9890" />
</a>
</li>
<li class='span1'>
<a class='thumbnail'>
<img alt="8b9890" src="http://dummyimage.com/100x100/fff/8b9890" />
</a>
</li>
<li class='span1'>
<a class='thumbnail'>
<img alt="8b9890" src="http://dummyimage.com/100x100/fff/8b9890" />
</a>
</li>
<li class='span1'>
<a class='thumbnail'>
<img alt="8b9890" src="http://dummyimage.com/100x100/fff/8b9890" />
</a>
</li>
<li class='span1'>
<a class='thumbnail'>
<img alt="8b9890" src="http://dummyimage.com/100x100/fff/8b9890" />
</a>
</li>
<li class='span1'>
<a class='thumbnail'>
<img alt="8b9890" src="http://dummyimage.com/100x100/fff/8b9890" />
</a>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class='deep-footer'>
<div class='container'>
<div class='row'>
<div class='span6'>
<div class='copyright'>Copyright &copy; 2012 Tamerlan Soziev. All rights reserved.</div>
</div>
<div class='span6'>
<ul class='footer-links'>
<li>
<a>Some</a>
</li>
<li>
<a>Footer</a>
</li>
<li>
<a>Policy</a>
</li>
<li>
<a>Terms Of Use</a>
</li>
<li>
<a>Links</a>
</li>
</ul>
</div>
</div>
</div>
</div>
</footer>

        <script src="../assets/theme_venera_manifest.js" type="text/javascript"></script>

        
    </body>
</html>