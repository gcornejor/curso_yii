<?php

class EscuelaController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Escuela'),
		));
	}

	public function actionCreate() {
		$model = new Escuela;

		$this->performAjaxValidation($model, 'escuela-form');

		if (isset($_POST['Escuela'])) {
			$model->setAttributes($_POST['Escuela']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Escuela');

		$this->performAjaxValidation($model, 'escuela-form');

		if (isset($_POST['Escuela'])) {
			$model->setAttributes($_POST['Escuela']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Escuela')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Escuela');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new Escuela('search');
		$model->unsetAttributes();

		if (isset($_GET['Escuela']))
			$model->setAttributes($_GET['Escuela']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}