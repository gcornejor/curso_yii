<?php

class AlumnoController extends GxController {


	public function actionView($id) {
		$this->render('view', array(
			'model' => $this->loadModel($id, 'Alumno'),
		));
	}
        
        /*public function actionVer($id) {
		$this->render('ver', array(
			'model' => $this->loadModel($id, 'Alumno'),
		));
	}*/

	public function actionCreate() {
		$model = new Alumno;


		if (isset($_POST['Alumno'])) {
			$model->setAttributes($_POST['Alumno']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'Alumno');


		if (isset($_POST['Alumno'])) {
			$model->setAttributes($_POST['Alumno']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'Alumno')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Alumno');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

        public function actionVertodos(){ //nombre en url
            $modelo_alumno = new Alumno('search'); //ambiente de búsqueda
            $modelo_alumno->unsetattributes(); //limpiar atributos de busqueda
            //$modelo_alumno->nombre = 'Curso_yii';
            if(isset($_GET['Alumno']))
                $modelo_alumno->attributes=$_GET['Alumno'];
            
            $this->render('ver_todos',array('alumno'=>$modelo_alumno)); //cargar y mostrar vista, por defecto se mete a la carpeta alumno
            //$this->renderPartial();
        }
	public function actionAdmin() {
		$model = new Alumno('search');
		$model->unsetAttributes();

		if (isset($_GET['Alumno']))
			$model->setAttributes($_GET['Alumno']);

		$this->render('admin', array(
			'model' => $model,
		));
	}
        
        public function actionMirar($id){
            $modelo_alumno=$this->loadModel($id, 'Alumno');
            $this->render('ver', array('alumno'=>$modelo_alumno));
        }


        /*public function actionVerTodos() {
		$model = new Alumno('search');
		$model->unsetAttributes();

		if (isset($_GET['Alumno']))
			$model->setAttributes($_GET['Alumno']);

		$this->render('verTodos', array(
			'model' => $model,
		));
	}*/


}