<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('escuela_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->escuela)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('nombre')); ?>:
	<?php echo GxHtml::encode($data->nombre); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('sexo')); ?>:
	<?php echo GxHtml::encode($data->sexo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('curso')); ?>:
	<?php echo GxHtml::encode($data->curso); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('rut')); ?>:
	<?php echo GxHtml::encode($data->rut); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('apellido_paterno')); ?>:
	<?php echo GxHtml::encode($data->apellido_paterno); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('apellido_materno')); ?>:
	<?php echo GxHtml::encode($data->apellido_materno); ?>
	<br />
	*/ ?>

</div>