<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php /*$this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'tabla-alumno',
	'dataProvider' => $alumno->search(),
	'filter' => $alumno,
	'columns' => array(
                
		//'id',
		/*array(
				'name'=>'escuela_id',
				'value'=>'GxHtml::valueEx($data->escuela)',
				'filter'=>GxHtml::listDataEx(Escuela::model()->findAllAttributes(null, true)),
				),
		//'nombre',
                array('name'=>'nombre',filter=>false), //no filtrar por nombre
                'rut',
		//'sexo',
		//'curso',
		'apellido_paterno',
		'apellido_materno',
		/*array(
			'class' => 'CButtonColumn',
		),
	),
)); */?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'tabla-alumno', //id tabla
'dataProvider'=>$alumno->search(), //proveedor de datos
'filter'=>$alumno, //filto
'type'=>'striped',//css tabla
'template'=>'{pager}{items}{summary}', //despliege de items
'columns'=>array(
//'id',
array('name'=>'nombre'),
    'apellido_paterno',
    'apellido_materno',
'rut',
'sexo',
'curso',
//'direccion',
//'ciudad',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
'template' => '{ver} {update} {borrar}',
'buttons' => array(
'ver' => array(
'label'=> 'Ver',
    'url'=>'Yii::app()->createUrl("alumno/mirar", array("id"=>$data->id))',
'options'=>array(
'class'=>'btn-info btn-small view'
)
), 
'update' => array(
'label'=> 'Actualizar',
'options'=>array(
'class'=>'btn-warning btn-small update'
)
),
'borrar' => array(
'label'=> 'Eliminar',
    'icon'=> 'trash', //editar icono
'options'=>array(
'class'=>'btn-danger btn-small delete'
)
)
),
'htmlOptions'=>array('style'=>'width: 115px'),
)
),
)); ?>
